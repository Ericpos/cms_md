﻿CREATE TABLE [dbo].[MD_Store]
(
	[Datasource] [nvarchar](25) NULL,
	[Chain] [nvarchar](25) NULL,
	[StoreKey] [bigint] NOT NULL,
	[StoreAlternateKey] [nvarchar](100) NULL,
	[EDI] [nvarchar](30) NULL,
	[DataSourceKey] [int] NULL,
	[Ignore] [bit] NULL,
	[NielsenCode] [nvarchar](25) NULL,
	[Store] [nvarchar](100) NULL,
	[Region] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[County] [nvarchar](100) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[SalesArea] [nvarchar](255) NULL,
	[CheckOuts] [int] NULL,
	[StoreClass] [nvarchar](100) NULL,
	[PotentialGroup] [nvarchar](50) NULL,
	[Latitude] [nvarchar](25) NULL,
	[Longitude] [nvarchar](25) NULL,
	[Telephone] [nchar](50) NULL,
	[Atributo1] [nvarchar](50) NULL,
	[Atributo2] [nvarchar](50) NULL,
	[Atributo3] [nvarchar](50) NULL,
	[Atributo4] [nvarchar](50) NULL,
	[CAPSAPromoChain] [nvarchar](50) NULL,
	[Atributo5] [nvarchar](100) NULL,
	[Atributo6] [nvarchar](100) NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[Updated] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[id_cronjobdetail] [int] NULL,
	[BatchID] [int] NULL,
	[UpdatedAtributo5] [datetime] NULL,
	[UpdatedAtributo5By] [nvarchar](100) NULL,
 CONSTRAINT [PK_DimStore] PRIMARY KEY CLUSTERED 
(
	[StoreKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MD_Store] ADD  CONSTRAINT [DF_DimStore_Updated]  DEFAULT (getdate()) FOR [Updated]
GO

ALTER TABLE [dbo].[MD_Store] ADD  CONSTRAINT [DF_DimStore_UpdatedAtributo5]  DEFAULT (getdate()) FOR [UpdatedAtributo5]
GO