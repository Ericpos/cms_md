﻿CREATE TABLE [dbo].[MD_Product](
	[sku_product] [bigint] IDENTITY(1,1) NOT NULL,
	[Productkey] [int] NOT NULL,
	[Product] [nvarchar](100) NULL,
	[EAN] [nvarchar](13) NOT NULL,
	[EANName] [nvarchar](120) NULL,
	[CustomerCode] [nvarchar](20) NULL,
	[Brand] [nvarchar](50) NULL,
	[SubBrand] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
	[SubCategory] [nvarchar](50) NULL,
	[Segment] [nvarchar](50) NULL,
	[SubSegment] [nvarchar](50) NULL,
	[Weight] [real] NULL,
	[Units] [real] NULL,
	[Price] [money] NULL,
	[ProdCompe] [bit] NULL,
	[Fabricante] [nvarchar](50) NULL,
	[TipoProd] [nvarchar](50) NULL,
	[Atributo1] [nvarchar](50) NULL,
	[Atributo2] [nvarchar](50) NULL,
	[Atributo3] [nvarchar](50) NULL,
	[Atributo4] [nvarchar](50) NULL,
	[Atributo5] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Version] [int] NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[ID_CronJobDetail] [int] NOT NULL,
 CONSTRAINT [PK_DimProduct] PRIMARY KEY CLUSTERED 
(
	[sku_product] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MD_Product] ADD  CONSTRAINT [DF_DimProduct_Created]  DEFAULT (getdate()) FOR [Created]
GO


